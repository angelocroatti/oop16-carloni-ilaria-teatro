package model;

/**
 * Interfaccia che comprende i i metodi necessari alla login
 * 
 * @author Ilaria Carloni
 * 
 */
public interface ILogin {

	public String getUsername();

	public String getPassword();
}
